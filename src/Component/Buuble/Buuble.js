import React, { Component } from 'react'
import { bubble as Menu } from 'react-burger-menu'
import { ProductConsumer } from '../../Context';
import {LanguageApi} from '../LanguageApi/LanguageApi'
import {Map} from '../Map/Map'
import {Button,Modal} from 'react-bootstrap'
export default class Buuble extends Component {
  showSettings (event) {
    event.preventDefault();
    
   
  }
 
  render () {
    return (
        <ProductConsumer>
            {((value) => {
                const {languageHanderClick,show,googlemapshow,inputmapclick,handleShow,handleClose,locationmap,LagBack,LagName,Limage,langMenu,langcheckedFuntcion,languageHanderBack,langchecked } = value;
    return (
        <>
        <div >
        
      <Menu  >

{
  langMenu ?
  <>
        <div className="l-g-t" onClick={langcheckedFuntcion}>{LagName}
        <img className="mkl-img" src={Limage} alt="lo"/>

        </div>
        <a   className=" bm-item menu-item--small loplp klop" >Trending</a>
        <a   className="bm-item menu-item--small loplp klop" >Discover</a>
        <a    className="bm-item menu-item--small loplp klop">My Hair Type</a>
        <a   className="bm-item menu-item--small loplp klop" >Find A Artist</a>
        <a   className="bm-item menu-item--small loplp klop" >Virtual Salon</a>
        <div className="bm-item  lg-us menu-item--small loplp klop">
        <i class="far user-asw fa-user-circle"></i>
        <div>
         <span>Log In/Sign Up</span> 
        </div>
        </div>

        <div  onClick={handleShow} className="loc-fetch ">
        <div className="loc-signal"></div>
        
        {locationmap}
     
      </div>
        </>
 : null
}

        {langchecked ?
<>
<div className="l-g-t" onClick={languageHanderBack}>{LagBack}
        <img className="mkl-img" src={Limage} alt="lo"/>
</div>
{
          LanguageApi.map(l=>
            {
              return(
               <>
                <a  className="bm-item menu-item--small loplp okjg" onClick={()=>{languageHanderClick(l) }}>{l.lname}
                <div className="round">
    <input type="checkbox" id={l.id} checked={(LagName == l.lname ? true : false)} />
    <label for={l.id}></label>
  </div>
  </a>
         </>      
              )
            }
           
            )
          }
            </>
            :null
        }

      </Menu>
    </div>
    {googlemapshow?
  <Modal className="map-p" show={show} onHide={handleClose} animation={false}>
 <Modal.Body>
    <div className="container p-pl" > 
    <h1 className="country-h1">Select your city</h1>
    <p className="ind">INDIA</p> 
    <div className="map-j">
      {
Map.map(m=>{
return(
  <div className="sub-map-j"  onClick={()=>inputmapclick(m)}>
    <img src={m.Mimages} alt="country"/>
  <p >{m.Mname}</p>
  </div>
)
})
}
</div>
</div>
</Modal.Body>
</Modal>
  : null}
          </>
    );
  
})}
  </ProductConsumer>
            )
}
}
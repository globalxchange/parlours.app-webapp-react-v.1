import React, { Component } from 'react'
import {Navbar,Nav,NavDropdown,OverlayTrigger,ButtonToolbar,Popover,Form,DropdownButton,Dropdown } from 'react-bootstrap'
import Ren from '../../Img/ren.png'           
import {Link} from 'react-router-dom'
import { ProductConsumer } from '../../Context'
import {Saloon} from '../SaloonApi/SaloonAPi'
import Models from '../HomeNav/Edit'
import PlaymentMethod from '../HomeNav/PaymentMethods.js'
import Social from '../HomeNav/Socail'
import './HomeNavFliter.css'
export default class HomeNavFliter extends Component {
  
    render() {
      return (
        <ProductConsumer>
                       {((value) => {
                          const {NamehandleClick1,NamehandleClick,NamehandleClick5,result1,name,type, NamehandleClick7,NamehandleClick2,NamehandleClick6} = value;
                          let pp = Saloon.filter( (ele, ind) => ind === Saloon.findIndex( elem => elem.categorytype === ele.categorytype))
        return (
          <>
          <div className="ka-sea">
                <Navbar  collapseOnSelect expand="lg"   variant="dark">
<Navbar.Brand >  <Link to="/" >  <img className="Ren" src={Ren} alt="logo"/></Link></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
     < div className="">
       <div className="search-section-fliter">
       <div className="qw1">
<ButtonToolbar>
  {[ 'bottom'].map(placement => (
    <OverlayTrigger
      trigger="click"
      rootClose={true}
      key={placement}
      placement={placement}
      overlay={
        <Popover className="popsearch" id={`popover-positioned-${placement}`}>
          {/* <Popover.Title as="h3">{`Popover ${placement}`}</Popover.Title> */}
          <Popover.Content>
          <div className="category1 ">
                                {
                                    pp.map(c=>{
                                        return(

                                            <>
                                            <div className="ka">
                                            <Link to="/Fliter"  onClick={()=>NamehandleClick(c)} >  
                                            <h5 className="ca">{c.categorytype} </h5>
                                         </Link>
                                            </div>


                                            </>
                                        )
                                    })
                                }

<div className="search-field">
                                {
                                  result1.map(r=>{
                                    return(
                                      <div className="d-flex">
                                          <Link to="/Fliter"  onClick={()=>NamehandleClick2(r)} > 
                                          <div className="d-flex mb-3" >
                                       
                                          <img className="res-img" src={r.image.img1} alt=""/> 
                                          <h5 className="iot">{r.name}</h5>
                                          </div>
                                             
                                          </Link>
                                      </div>
                                     
                                    )
                                  })
                                }
                                </div>
                            </div>
          </Popover.Content>
        </Popover>
      }
    >
        <div>
          <div className="aws">
          <svg className="hello" viewBox="0 0 24 24"><path d="M20.57 20.42l-5-5.25a7.14 7.14 0 1 0-.5.46l5.06 5.26a.34.34 0 1 0 .49-.47zm-10.1-3.83a6.46 6.46 0 1 1 6.46-6.46 6.46 6.46 0 0 1-6.46 6.46z"></path></svg>
          </div>
        <Form.Control size="lg" type="text" className="inpu" onChange={NamehandleClick1} placeholder="Search For a Service or Venue" />
       
        </div>
      {/* <Button variant="secondary">Popover on {placement}</Button> */}
    </OverlayTrigger>
  ))}
</ButtonToolbar>

{/*  */}

    </div>
    
    <div className="qw">
 <ButtonToolbar>
  {[ 'bottom'].map(placement => (
    <OverlayTrigger
      trigger="click"
      key={placement}
      rootClose={true}
      placement={placement}
      overlay={
        <Popover className="popsearch1" id={`popover-positioned-${placement}`}>
          {/* <Popover.Title as="h3">{`Popover ${placement}`}</Popover.Title> */}
          <Popover.Content>
          <div className="category ">
          <div className="aws">
          <svg className="hello" viewBox="0 0 24 24"><path d="M12.06 6.94a2.3 2.3 0 1 0 2.3 2.3 2.3 2.3 0 0 0-2.3-2.3zm0 3.94a1.64 1.64 0 1 1 1.64-1.63 1.65 1.65 0 0 1-1.64 1.64z"></path><path d="M16.46 4.85a6.3 6.3 0 0 0-8.92 0A7.18 7.18 0 0 0 7 13.71L12 21l5-7.28a7.18 7.18 0 0 0-.54-8.87zm.05 8.49L12 19.84l-4.52-6.52a6.49 6.49 0 0 1 .52-8 5.65 5.65 0 0 1 8 0 6.49 6.49 0 0 1 .51 8.01z"></path></svg>
        <h4>Current location</h4>
        </div>
            
        </div>
      
          </Popover.Content>
        </Popover>
      }
    >
        <div>
        <div className="aws">
        <svg className="hello" viewBox="0 0 24 24"><path d="M12.06 6.94a2.3 2.3 0 1 0 2.3 2.3 2.3 2.3 0 0 0-2.3-2.3zm0 3.94a1.64 1.64 0 1 1 1.64-1.63 1.65 1.65 0 0 1-1.64 1.64z"></path><path d="M16.46 4.85a6.3 6.3 0 0 0-8.92 0A7.18 7.18 0 0 0 7 13.71L12 21l5-7.28a7.18 7.18 0 0 0-.54-8.87zm.05 8.49L12 19.84l-4.52-6.52a6.49 6.49 0 0 1 .52-8 5.65 5.65 0 0 1 8 0 6.49 6.49 0 0 1 .51 8.01z"></path></svg>
        </div>
        <Form.Control size="lg" type="text" className="inpu1"  placeholder="Choose Your Location" />
    </div>
      {/* <Button variant="secondary">Popover on {placement}</Button> */}
    </OverlayTrigger>
  ))}
</ButtonToolbar>
    </div>
    </div> 
<div className="drps  ">
<DropdownButton id="dropdown-basic-button " title="Top Rated">
  <Dropdown.Item >Top Rated</Dropdown.Item>
  <Dropdown.Item >Nearest</Dropdown.Item>
  <Dropdown.Item >Newest</Dropdown.Item>
  <Dropdown.Item>Lowest</Dropdown.Item>
</DropdownButton >
<DropdownButton id="dropdown-basic-button  " title={type}>
  <Dropdown.Item onClick={NamehandleClick7} >Unisex</Dropdown.Item>
  <Dropdown.Item onClick={NamehandleClick5}>Men</Dropdown.Item>
  <Dropdown.Item onClick={NamehandleClick6}>Women</Dropdown.Item>
</DropdownButton>

</div>
</ div>
    </Nav>
    <Nav> 
 
    <NavDropdown className="drp-p"  id="collasible-nav-dropdown">
        <NavDropdown.Item ><Models/></NavDropdown.Item>
        <NavDropdown.Item ><Social/></NavDropdown.Item>
        <NavDropdown.Item ><PlaymentMethod/></NavDropdown.Item>
        <NavDropdown.Item >Appointments</NavDropdown.Item>
        <NavDropdown.Item >Favorites</NavDropdown.Item>
        <NavDropdown.Item >List Your Salon</NavDropdown.Item>
        <NavDropdown.Item >Login</NavDropdown.Item>
      </NavDropdown>
    </Nav>
  </Navbar.Collapse>
</Navbar>

</div>

{/*  */}






      {/*  */}

      


           </>
        )

                       })}
                         </ProductConsumer>  
                       
                       )
    }
}

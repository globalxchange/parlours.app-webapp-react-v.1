import React, { Component } from 'react'
import './Footer.css'
export default class Footer extends Component {
    render() {
        return (
            <div className="container">
                <div className="f-t-main" >
                    <div className="f-t-w">
<p>Copyright 2019. All Rights Reserved. Rendezvous Technology Group Inc.</p>
                    </div>
                    <div className="f-t">
                        <p>Privacy Policy</p>
                        <p>Website Terms</p>
                        <p>Booking Terms</p>
                    </div>
                    <div>

                    </div>
                </div>
            </div>
        )
    }
}

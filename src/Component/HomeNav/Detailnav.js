import React, { Component } from 'react'
import {Navbar,Nav,NavDropdown} from 'react-bootstrap'
import Ren from '../../Img/ren.png'
import './HomeNav.css'
import { ProductConsumer } from '../../Context'
import {Button,Modal} from 'react-bootstrap'
import Models from './Edit'
import PlaymentMethod from './PaymentMethods.js'
import {Link} from 'react-router-dom'
import Social from './Socail'
export default class DetailNav extends Component {
    render() {
      return (
        <ProductConsumer>
                       {((value) => {
                          const {handleClose,handleShow,show} = value;
        return (
          <>
                <Navbar  collapseOnSelect expand="lg"  variant="dark">
<Navbar.Brand >  <Link to="/" > <svg class="back" viewBox="0 0 24 24"><path d="M8.48 12l8.13-8.13a.51.51 0 0 0-.72-.72l-8.5 8.49a.51.51 0 0 0 0 .72l8.49 8.49a.51.51 0 0 0 .36.15.5.5 0 0 0 .36-.15.51.51 0 0 0 0-.72z"></path></svg></Link></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
    </Nav>
    <Nav> 
    <Nav.Link >Appointments
</Nav.Link>
<Nav.Link >
Favorites
</Nav.Link>
    <NavDropdown className="drp"  id="collasible-nav-dropdown">
        <NavDropdown.Item ><Models/></NavDropdown.Item>
        <NavDropdown.Item ><Social/></NavDropdown.Item>
        <NavDropdown.Item ><PlaymentMethod/></NavDropdown.Item>
        <NavDropdown.Item >List Your Salon</NavDropdown.Item>
        <NavDropdown.Item >Login</NavDropdown.Item>
      </NavDropdown>
    </Nav>
  </Navbar.Collapse>
</Navbar>



{/*  */}






      {/*  */}

      


           </>
        )

                       })}
                         </ProductConsumer>  
                       
                       )
    }
}

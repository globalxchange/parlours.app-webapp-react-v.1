

import {Modal,Button,Form} from 'react-bootstrap';
import React, { Component } from 'react'
import { ProductConsumer } from '../../Context';
import PlaymentMethods from '../../Img/payment.png'


export default class Edit extends Component {

state=
{
show:false,

}


handleShow  = () => {
  this.setState({show:true,
 
  });

}

 handleClose  = () => {
  this.setState({show: false,

  
  });
}


  render() {
    return (
      <ProductConsumer>
                     {((value) => {
                        const {inputValue1,handleSubmit} = value;
    return (
      <>
  <span onClick={this.handleShow}>
  PlaymentMethods
        </span>
   <Modal className="sss" show={this.state.show} onHide={this.handleClose} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title  >PlaymentMethods</Modal.Title>
          </Modal.Header>
          <div className="edit-selection mt-3" >
          <Form onSubmit={handleSubmit}>
  <Form.Group >
  <Form.Control type="text" placeholder="Car Holder Name" onChange={inputValue1} />
  </Form.Group>
  <img  className="pop"
    src={PlaymentMethods} alt="logo"/>
  <Form.Group >
  <Form.Control type="text" placeholder="Credit or Debit card Number" onChange={inputValue1} />
  </Form.Group>
<Form.Group >
    
  <section className="my-2">

<div className="mt-3  inputBoxStyle">
<Form.Group >
<Form.Control type="text" placeholder="MM/YY" onChange={inputValue1} />
  </Form.Group>

  <Form.Group >
<Form.Control type="text" placeholder="CVV" onChange={inputValue1} />
  </Form.Group>
</div>
</section>
 
  </Form.Group>
  
</Form>

          </div>
          <Modal.Footer>
          <Button variant="primary btn-edit" type="submit"  >
 Save PlaymentMethods
  </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
  })}
  </ProductConsumer>
    )
  }
}




import {Modal,Button,Form} from 'react-bootstrap';
import React, { Component } from 'react'
import { ProductConsumer } from '../../Context';
import Men from '../../Img/meen.png'
import {countryCodeApi} from '../CountryCodeApi/CountryCodeApi'
function buildFileSelector(){
  const fileSelector = document.createElement('input');
  fileSelector.setAttribute('type', 'file');
  fileSelector.setAttribute('multiple', 'multiple');
  return fileSelector;
}
export default class Edit extends Component {

state=
{
show:false,

}
componentDidMount(){
  this.fileSelector = buildFileSelector();
}

handleFileSelect = (e) => {
  e.preventDefault();
  this.fileSelector.click();
}


handleShow  = () => {
  this.setState({show:true,
 
  });

}

 handleClose  = () => {
  this.setState({show: false,

  
  });
}


  render() {
    return (
      <ProductConsumer>
                     {((value) => {
                        const {inputValue1,handleSubmit} = value;
    return (
      <>
          <div className="edit-selection" >
  <span onClick={this.handleShow}>
         Edit Profile
        </span>
   <Modal className="sss" show={this.state.show} onHide={this.handleClose} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title >Edit Profile</Modal.Title>
          </Modal.Header>
          <div className="edit-selection" >
          <div className="img-edit-section">
            <img className="img-edit" src={Men} alt="logo"/>
          </div>
          <p className="button changeprofile" href="" onClick={this.handleFileSelect}>Change Profile Picture</p>
          <Form onSubmit={handleSubmit}>
  <Form.Group >
  <Form.Control type="text" placeholder="Frist Name" onChange={inputValue1} />
  </Form.Group>
  <Form.Group >
  <Form.Control type="text" placeholder="Last Name" onChange={inputValue1} />
  </Form.Group>
  <Form.Group >
  <Form.Control type="text" placeholder="Email" onChange={inputValue1} />
  </Form.Group>

  <Form.Group >
  <section className="my-2">

<div className="mt-3  inputBoxStyle">
<select className="countryCode"  name="countryCode"  required>
{
    countryCodeApi.map(countryCode=>{
        return (
            <option key={countryCode.dial_code+Math.random()} value={countryCode.dial_code}>{countryCode.name+" "+countryCode.dial_code}</option>
        )
    })
}
</select>
    <input type="number" placeholder="Enter Your Phone Number" name="phoneNumber" onChange={inputValue1} autoComplete="off" className="form-control customFormControlSelect" required/>
</div>
</section>
 
  </Form.Group>
  
</Form>

          </div>
          <Modal.Footer>
          <Button variant="primary btn-edit" type="submit"  >
    Submit
  </Button>
          </Modal.Footer>
        </Modal>
        </div>
      </>
    )
  })}
  </ProductConsumer>
    )
  }
}


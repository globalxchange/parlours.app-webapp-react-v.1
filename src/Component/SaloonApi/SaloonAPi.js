import room from '../../Img/room.jpg'
import chair from '../../Img/chair.jpg'
import green from '../../Img/green.jpg'
import fit from '../../Img/fit.jpg'
import spa from '../../Img/spa.jpg'
import light from '../../Img/light.jpg'
import eye from '../../Img/eye.jpg'
import hair from '../../Img/hair.jpg'
import ac from '../../Img/ac.jpg'
import nail from '../../Img/nail.svg'
import Massage from '../../Img/massage.svg'
import app from '../../Img/app.svg'
import clean from '../../Img/app.svg'
import set from '../../Img/set.svg'
import heart from '../../Img/heart.svg'
import S1 from '../../Img/s1.png'
import S2 from '../../Img/s2.png'
import S3 from '../../Img/s3.png'
import S4 from '../../Img/s4.png'
import S5 from '../../Img/s5.jpg'
import S6 from '../../Img/s6.jpg'
import F1 from '../../Img/f1.png'
import  F2 from '../../Img/f2.jpg'
import F3 from '../../Img/f3.jpg'
import M1 from '../../Img/m1.png'
import M2 from '../../Img/m2.jpg'
import M3 from '../../Img/m3.jpg'
import M4 from '../../Img/m4.jpg'
import M5 from '../../Img/m5.jpg'
import StraightHair from '../../Img/striaghtmen.jpg'
import wavy from '../../Img/wavy.jpg'
import afro from '../../Img/afro.png'
import Curly from '../../Img/Curly.jpg'
import swomen from '../../Img/swomen.jpg'
import wwomen from '../../Img/wwomen.jpg'
import cwomen from '../../Img/cwomen.png'
import cowomen from '../../Img/cowemen.jpg'
import kkid from '../../Img/kkid.jpg'
import ckid from '../../Img/ckid.jpg'
import wkid from '../../Img/wkid.jpg'
import skid from '../../Img/skid.jpg'
import mcoming from '../../Img/mcoming.jpg'
import wcoming from '../../Img/wcoming.jpg'
import kcoming from '../../Img/kcoming.jpg'

   // Trending Near You  id till -5 
   //  Categorylistmain:'Men',till-8`
   //Trims till-14
//Facials till-17

export const Saloon = [

    {
      id:'1',
      locationstreet:"Jp Nagar",
      ischecked:false,
      titlecagory:'Ines Lobo',
      name: 'Stunnings Beauty Salon and Spa',
      Categorylistmain:'Trending Near You',
      type: 'Unisex',
      Booking:'Instant Booking',
      clocks:'open to 10am',
      catimage:nail,
     address:'83/5 1st floor chambenhalli gate sarjapura main road bengaluru Karnataka India, bangalore',
     pincode:'563101',
     categorytype:'Hair',
     About:'Stunnings Beauty Salon gate sarjapura Spa road bengaluru Karnataka India,',
     service:'348',
     googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
      weekdays:[
         {

            name:'Monday',
            time:'10:00am - 9:00pm'
         },
         {

            name:'Tuesday',
            time:'10:00am - 9:00pm'
         },
         {

            name:'Wednesday',
            time:'10:00am - 9:00pm'
         },
         {

            name:'Thursday',
            time:'10:00am - 9:00pm'
         },
         {

            name:'Friday',
            time:'10:00am - 9:00pm'
         },
         {

            name:'Saturday',
            time:'10:00am - 9:00pm'
         },
         {

            name:'Sunday',
            time:'10:00am - 9:00pm'
         },
     ],
    Hairservice:[
        {
      
            name:'Eye Brow Threading',
            price:20,
        },
       
         {
            name:'Cheryles Derma shade (50) SPF',
            price:600,
        },
        {
            name:'Cheryls DermaLite',
            price:300,
        },
        {
            name:'Loreal Prodect',
            price:900,
        },
        {
            name:'INOVA POST MASK',
            price:40,
        },
        {
            name:'INOVA POST COLOUR SHAMPOO',
            price:100,
        }
     ],
  MassageTherapy:
   [
      {
      name:'5 Min Express Massage therapy',
      price:2500,
      },
      {
         name:'Signature Massage',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:550,
      },
      {
         name:'Signature Massage',
         price:2500, 
      }
   ],
timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
   HygieneTherapy:
   [
      {
         name:'Hair Spa',
         price:250,
         },
         {
            name:'Pedicure',
            price:250,
         },
         {
            name:'Four Hand Hawaiian',
            price:50,
         },
         {
            name:'Manicure',
            price:2500, 
         }
   ],
   Body:
   [
      {
         name:'Body Wrap',
         price:2500,
         },
         {
            name:'Body Scrub',
            price:250,
         },
         
   ],
     image:{
        img1:room,
        img2:eye,
        img3:green,
        img4:ac,
    }
},

 
{
    id:'2',
    ischecked:false,
    locationstreet:"Jayanagar",
    titlecagory:'Ines Lobo',
     name: 'Sprouts Salon Spa',
     type: 'Unisex',
     Categorylistmain:'Trending Near You',
     Booking:'Instant Booking',
     clocks:'open to 10am',
    address:'Sprouts Salon & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
    pincode:'',
    About:'Sprouts Salon & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra,',
    categorytype:'Beard',
    catimage:heart,
     service:'245',
     timings:[
         {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
     googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
     image:{
        img1:chair,
        img2:eye,
        img3:green,
        img4:ac,
    },
    weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
},
{
    id:'3',
    ischecked:false,
    locationstreet:"Electronic City",
    titlecagory:'Ines Lobo',
     name: 'Eastern Salon & Spa',
     type: 'Unisex',
     Categorylistmain:'Trending Near You',
     Booking:'Instant Booking',
     catimage:Massage,
     clock:'open to 10am',
     
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    address:'# 1666, 1st Floor, 9th main, HAL 3rd stage, near BSNL office, Bangalore, 560075',
    pincode:'',
    categorytype:'Massage',
     service:'125 ',
     About:'Eastern Salon Spa, Indira Nagar & HBR Layout',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:green,
        img2:eye,
        img3:fit,
        img4:ac,
    }
},
{
    id:'4',
    ischecked:false,
    locationstreet:"HSR Layout",
    titlecagory:'Ines Lobo',
     name: 'Lavish Looks Slimming & Beauty Unisex Studio',
     type: 'Unisex',
     Categorylistmain:'Trending Near You',
     Booking:'Instant Booking',
     catimage:app,
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
     clock:'open to 10am',
    categorytype:'Apparel',
    address:'155, 1st floor Sarjapur road kodathi gate., Opposite Shell Petrol Bunk, Bengaluru, 5600355',
    pincode:'',
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
     service:'155 ',
     About:'Lavish Looks is an organization of health and wellness specialists. Our mission is to help you discover inner and outer beauty and achieve sound health and fitness goals. As a pioneer of holistic well-being concept, Lavish Looks offers most advanced treatment and therapies to relax and revive. Our expertise is in diagnosing obesity, skin and hair related issues, often caused by lifestyle choices or medical side effects and hormonal changes. With a state of the art facilities and a professional team of Doctors, Physiotherapists, Cosmetologists, and Nutritionists, we assure our customers complete satisfaction. Our services include Instant Inch Loss (Technique for Figure Correction) Weight Loss (for Health and Fitness), Hair & Skin treatments including Hair loss Control and Repair, Microdermabrasion, Stretch Marks Removal, Removal of Under eye Darkness, Glycolic peels, all regular and advanced parlor and salon services for Men and Women.t',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:fit,
        img2:eye,
        img3:green,
        img4:ac,
    }
    },
{
    id:'5',
    titlecagory:'Ines Lobo',
    locationstreet:"Jp Nagar",
     name: 'PoornaAyur @ Race Course',
     type: 'Unisex',
     ischecked:false,
      Categorylistmain:'Trending Near You',
     Booking:'Instant Booking',
     clock:'open to 10am',
     catimage:clean,
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
    categorytype:'Massage',
    address:'Race Course Road, No 128, Between Orga Foods & RK Photo Centre, Coimbatore, 641018',
    pincode:' 641018',
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
     service:' 500',
     bookingtype:'Instant Booking',
     About:'PoornaAyur is an Ayurvedic Clinic/ Wellness Centre/ Panchakarma Treatment Centre situated in Race Course, Coimbatore & Mahalingapuram, Pollachi. We offer the entire range of Ayurvedic Treatments for Stress Relief, Knee Pain, Joint Pain, Back Pain, Neck Pain, Paralysis, Frozen Shoulder, Weight Loss Treatments, Detox Treatments, Rheumatism, Arthritis , Pre-Bridal Readiness Packages, Hair & Skin Care Treatments. All the treatments are done by certified and trained Ayurvedic Professionals supervised by Certified Ayurvedic Doctor. We also have a full fledged Ayurvedic Pharmacy of Kottakkal Arya Vaidya Sala.You can call us direct for appointments on free consultation.',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:spa,
        img2:eye,
        img3:green,
        img4:ac,
    }
    },

  
{
    id:'6',
    Categorylistmain:'Men',
    locationstreet:"Jp Nagar",
    categorylisttype:'Straight',
    categorylisttypeimg:StraightHair,
    name: 'XSALONCE Unisex Salon & Spa',
     type: 'Unisex',
     ischecked:false,
     titlecagory:'Ines Lobo',
  Booking:'Instant Booking',
     clock:'open to 10am',
     catimage:set,
    address:'No 26/1, Thiruverkadu Road, Ayapakkam, Ambattur, Chennai, 600077',
    pincode:'',



    
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
    categorytype:'Products',
     service:'155 ',
     googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
     About:'Book your services and avail the Online offers from the outles or get 25% Off on any services by booking online. Xsalonce Unisex Salon & Spa provides you the ultimate beauty experience. Their aim is to make you feel better and look beautiful. They have a team of professionals who are always at your service to provide you guidance and treatments according to your skin requirements. They also use premium products and best equipment to ensure quality service in every sense.',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
        
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:light,
        img2:eye,
        img3:green,
        img4:ac,
    }
    },
{
    id:'7',
     name: 'SINIMA Professional Hair & Bridal Studio',
     locationstreet:"Jp Nagar",
     titlecagory:'Ines Lobo',
     type: 'Unisex',
     ischecked:false,
     Categorylistmain:'Men',
     categorylisttype:'Wavy',
     categorylisttypeimg:wavy,
     Booking:'Instant Booking',
     clock:'open to 10am',
     categorytype:'Products',
     
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
     googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    address:'155, 1st floor Sarjapur road kodathi gate., Opposite Shell Petrol Bunk, Bengaluru, ',
    pincode:'5600355',
     service:'108  ',
     About:'Unisex Salon',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:eye,
        img2:fit,
        img3:green,
        img4:ac,
    }

    },
{
    id:'8',
     name: 'OneTouch Hair & Beauty',
     locationstreet:"Jp Nagar",
     titlecagory:'Ines Lobo',
     type: 'Women',
     ischecked:false,
     Categorylistmain:'Men',
     categorylisttype:'Curly',
     categorylisttypeimg:Curly,
     catimage:heart,
     Booking:'Instant Booking',
     clock:'open to 10am',
     
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
    address:'Mangattukadavu Thirumala Road, OneTouch Hair & Beauty (Near SBI ATM), Thiruvananthapuram, ',
    pincode:'695006',
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    categorytype:'Massage',
     service:'155 ',
     
     About:'OneTouch Hair and Beauty Spa',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:S6,
        img2:eye,
        img3:green,
        img4:ac,
    }
    },
{
    id:'9',
     name: 'GoldenGlowSalon',
     locationstreet:"Jp Nagar",
     ischecked:false,
     titlecagory:'Ines Lobo',
     type: 'men',
     Categorylistmain:'Women',
     categorylisttype:'Straight',
     categorylisttypeimg:swomen,
     catimage:Massage,
     Booking:'Instant Booking',
     clock:'open to 10am',
     
   timings:[

      {
         times:"10:00am"
      },
      {
         times:"11:00am"
      },
      {
         times:"12:00am"
      },
      {
         times:"1:00pm"
      },
      {
         times:"2:00pm"
      },
       {
         times:"3:00pm"
      },
       {
         times:"4:00pm"
      },
       {
         times:"5:00pm"
      },
       {
         times:"6:00pm"
      },
       {
         times:"7:00pm"
      },
       {
         times:"8:00pm"
      },
       {
         times:"9:00pm"
      },
       {
         times:"10:00pm"
      },
       {
         times:"11:00pm"
      },

      {
         times:"12:00pm"
      }

   ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    address:'Street Number 5, Golden Tulip Estate, JV Hills, Kondapur, Hyderabad,  ',
    pincode:'500084',
    categorytype:'Apparel',
     service:'155 ',
     About:'Beauty Salon',
     weekdays:[
        {

           name:'Monday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Tuesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Wednesday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Thursday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Friday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Saturday',
           time:'10:00am - 9:00pm'
        },
        {

           name:'Sunday',
           time:'10:00am - 9:00pm'
        },
    ],
    Hairservice:[
        
      {
   
         name:'Eye Brow Threading',
         price:20,
     },
    
      {
         name:'Cheryles Derma shade (50) SPF',
         price:600,
     },
     {
         name:'Cheryls DermaLite',
         price:300,
     },
     {
         name:'Loreal Prodect',
         price:900,
     },
     {
         name:'INOVA POST MASK',
         price:40,
     },
     {
         name:'INOVA POST COLOUR SHAMPOO',
         price:100,
     }
  ],

  MassageTherapy:
[
   {
   name:'5 Min Express Massage therapy',
   price:2500,
   },
   {
      name:'Signature Massage',
      price:250,
   },
   {
      name:'Four Hand Hawaiian',
      price:550,
   },
   {
      name:'Signature Massage',
      price:2500, 
   }
],
HygieneTherapy:
[
   {
      name:'Hair Spa',
      price:250,
      },
      {
         name:'Pedicure',
         price:250,
      },
      {
         name:'Four Hand Hawaiian',
         price:50,
      },
      {
         name:'Manicure',
         price:2500, 
      }
],
Body:
[
   {
      name:'Body Wrap',
      price:2500,
      },
      {
         name:'Body Scrub',
         price:250,
      },
      
],
     image:{
        img1:ac,
        img2:eye,
        img3:green,
        img4:ac,
    }
    },
   
{
   id:'10',
   titlecagory:'Ines Lobo',
   locationstreet:"Jp Nagar",
    name: 'Inspriations Hair & Beauty Saloon',
    ischecked:false,
    type: 'Unisex',
    Categorylistmain:'Trending Near You',
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'nspriations Hair & Beauty Saloon in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'nspriations Hair & Beauty Saloon in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:S1,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'11',
    name: 'Delightful, Delicious, De lovely ',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Unisex',
    Categorylistmain:'Women',
    categorylisttype:'Straight',
     categorylisttypeimg:swomen,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Delightful, Delicious, De lovely Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Delightful, Delicious, De lovely Maharashtra,',
   categorytype:'Beard',
   catimage:S2,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:S2,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'12',
    name: 'Infinity Hair Saloon',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Unisex',
    Categorylistmain:'Women',
    categorylisttype:'Wavy',
    categorylisttypeimg:wwomen,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Infinity Hair Saloon in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Infinity Hair Saloon in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:S3,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'13',
    name: 'Turkish Saloon',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Men',
    Categorylistmain:'Women',
    categorylisttype:'Curly',
    categorylisttypeimg:cwomen,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Turkish Saloon in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Turkish Saloon in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:S4,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'14',
    name: 'Salonreyna hair Saloon',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Women',
    Categorylistmain:'Women',
    categorylisttype:'Coily',
    categorylisttypeimg:cowomen,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Salonreyna hair Saloon Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Salonreyna hair Saloon Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:S5,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'15',
    name: 'Birbar Shop',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Men',
     Categorylistmain:'Trending Near You',
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Birbar Shop in  Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Birbar Shop in  Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:F1,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
id:'16',
 name: 'Lotus Saloon',
 locationstreet:"Jp Nagar",
 titlecagory:'Ines Lobo',
 ischecked:false,
 type: 'Unisex',
  Categorylistmain:'Trending Near You',
 Booking:'Instant Booking',
 clocks:'open to 10am',
address:'Lotus Saloon Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
pincode:'',
About:'Lotus Saloon Salon/Studio in Baner, Baner, Pune, Maharashtra,',
categorytype:'Beard',
catimage:heart,
 service:'245',
 timings:[
     {
     times:"10:00am"
  },
  {
     times:"11:00am"
  },
  {
     times:"12:00am"
  },
  
  {
     times:"1:00pm"
  },
  {
     times:"2:00pm"
  },
   {
     times:"3:00pm"
  },
   {
     times:"4:00pm"
  },
   {
     times:"5:00pm"
  },
   {
     times:"6:00pm"
  },
   {
     times:"7:00pm"
  },
   {
     times:"8:00pm"
  },
   {
     times:"9:00pm"
  },
   {
     times:"10:00pm"
  },
   {
     times:"11:00pm"
  },

  {
     times:"12:00pm"
  }

],
 googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
 image:{
    img1:F2,
    img2:eye,
    img3:green,
    img4:ac,
},
weekdays:[
    {

       name:'Monday',
       time:'10:00am - 9:00pm'
    },
    {

       name:'Tuesday',
       time:'10:00am - 9:00pm'
    },
    {

       name:'Wednesday',
       time:'10:00am - 9:00pm'
    },
    {

       name:'Thursday',
       time:'10:00am - 9:00pm'
    },
    {

       name:'Friday',
       time:'10:00am - 9:00pm'
    },
    {

       name:'Saturday',
       time:'10:00am - 9:00pm'
    },
    {

       name:'Sunday',
       time:'10:00am - 9:00pm'
    },
],
Hairservice:[
    
  {

     name:'Eye Brow Threading',
     price:20,
 },

  {
     name:'Cheryles Derma shade (50) SPF',
     price:600,
 },
 {
     name:'Cheryls DermaLite',
     price:300,
 },
 {
     name:'Loreal Prodect',
     price:900,
 },
 {
     name:'INOVA POST MASK',
     price:40,
 },
 {
     name:'INOVA POST COLOUR SHAMPOO',
     price:100,
 }
],

MassageTherapy:
[
{
name:'5 Min Express Massage therapy',
price:2500,
},
{
  name:'Signature Massage',
  price:250,
},
{
  name:'Four Hand Hawaiian',
  price:550,
},
{
  name:'Signature Massage',
  price:2500, 
}
],
HygieneTherapy:
[
{
  name:'Hair Spa',
  price:250,
  },
  {
     name:'Pedicure',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:50,
  },
  {
     name:'Manicure',
     price:2500, 
  }
],
Body:
[
{
  name:'Body Wrap',
  price:2500,
  },
  {
     name:'Body Scrub',
     price:250,
  },
  
],
},
{
   id:'17',
    name: 'Classic Salon Spa',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Unisex',
     Categorylistmain:'Trending Near You',
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Classic Salon & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Classic Salon & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:F3,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},

{

   id:'18',
   titlecagory:'Ines Lobo',
   locationstreet:"Jp Nagar",
   ischecked:false,
    name: 'Never End Salon Spa',
    type: 'Unisex',
    Categorylistmain:'Kids',
    categorylisttype:'Straight',
    categorylisttypeimg:skid,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Never End Salon Spa Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Never End Salon Spa Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:chair,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},

{
   id:'19',
    name: 'O2 & Spa',
    ischecked:false,
    titlecagory:'Ines Lobo',
    locationstreet:"Jp Nagar",
    type: 'Men',
    Categorylistmain:'Kids',
    categorylisttype:'Wavy',
    categorylisttypeimg:wkid,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'O2 & & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'O2 & & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:M2,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'20',
    name: ' Spa Nation',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    ischecked:false,
    type: 'Unisex',
    Categorylistmain:'Kids',
    categorylisttype:'Wavy',
    categorylisttypeimg:wkid,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Spa Nation & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Spa Nation & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:M3,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
{
   id:'21',
   titlecagory:'Ines Lobo',
   locationstreet:"Jp Nagar",
    name: 'Liquid Hair Spa',
    type: 'Unisex',
    ischecked:false,
    Categorylistmain:'Kids',
    categorylisttype:'Curly',
    categorylisttypeimg:ckid,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Liquid Hair Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Liquid Hair Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:M4,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',      
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},

{
   id:'22',
    name: 'SK Salon Spa',
    titlecagory:'Ines Lobo',
    locationstreet:"Jp Nagar",
    type: 'Unisex',
    ischecked:false,
    Categorylistmain:'Kids',
    categorylisttype:'Kinky',
    categorylisttypeimg:kkid,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'SK Salon & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'SK Salon & Spa - Unisex Beauty Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:M5,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},

{
   id:'23',
    name: 'OneTouch Hair & Beauty',
    locationstreet:"Jp Nagar",
    titlecagory:'Ines Lobo',
    type: 'Women',
    ischecked:false,
    Categorylistmain:'Men',
    categorylisttype:'Afro',
    categorylisttypeimg:afro ,
  
    catimage:heart,
    Booking:'Instant Booking',
    clock:'open to 10am',
    
  timings:[

     {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
   address:'Mangattukadavu Thirumala Road, OneTouch Hair & Beauty (Near SBI ATM), Thiruvananthapuram, ',
   pincode:'695006',
   googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
   categorytype:'Massage',
    service:'155 ',
    
    About:'OneTouch Hair and Beauty Spa',
    weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
    image:{
       img1:S6,
       img2:eye,
       img3:green,
       img4:ac,
   }
   },
   {
      id:'24',
       name: 'GoldenGlowSalon',
       ischecked:false,
       locationstreet:"Jp Nagar",
       titlecagory:'Ines Lobo',
       type: 'men',
       Categorylistmain:'Women',
       categorylisttype:'Coming Soon',
       categorylisttypeimg:wcoming,
       catimage:Massage,
       Booking:'Instant Booking',
       clock:'open to 10am',
       
     timings:[
  
        {
           times:"10:00am"
        },
        {
           times:"11:00am"
        },
        {
           times:"12:00am"
        },
        {
           times:"1:00pm"
        },
        {
           times:"2:00pm"
        },
         {
           times:"3:00pm"
        },
         {
           times:"4:00pm"
        },
         {
           times:"5:00pm"
        },
         {
           times:"6:00pm"
        },
         {
           times:"7:00pm"
        },
         {
           times:"8:00pm"
        },
         {
           times:"9:00pm"
        },
         {
           times:"10:00pm"
        },
         {
           times:"11:00pm"
        },
  
        {
           times:"12:00pm"
        }
  
     ],
      googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
      address:'Street Number 5, Golden Tulip Estate, JV Hills, Kondapur, Hyderabad,  ',
      pincode:'500084',
      categorytype:'Apparel',
       service:'155 ',
       About:'Beauty Salon',
       weekdays:[
          {
  
             name:'Monday',
             time:'10:00am - 9:00pm'
          },
          {
  
             name:'Tuesday',
             time:'10:00am - 9:00pm'
          },
          {
  
             name:'Wednesday',
             time:'10:00am - 9:00pm'
          },
          {
  
             name:'Thursday',
             time:'10:00am - 9:00pm'
          },
          {
  
             name:'Friday',
             time:'10:00am - 9:00pm'
          },
          {
  
             name:'Saturday',
             time:'10:00am - 9:00pm'
          },
          {
  
             name:'Sunday',
             time:'10:00am - 9:00pm'
          },
      ],
      Hairservice:[
          
        {
     
           name:'Eye Brow Threading',
           price:20,
       },
      
        {
           name:'Cheryles Derma shade (50) SPF',
           price:600,
       },
       {
           name:'Cheryls DermaLite',
           price:300,
       },
       {
           name:'Loreal Prodect',
           price:900,
       },
       {
           name:'INOVA POST MASK',
           price:40,
       },
       {
           name:'INOVA POST COLOUR SHAMPOO',
           price:100,
       }
    ],
  
    MassageTherapy:
  [
     {
     name:'5 Min Express Massage therapy',
     price:2500,
     },
     {
        name:'Signature Massage',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:550,
     },
     {
        name:'Signature Massage',
        price:2500, 
     }
  ],
  HygieneTherapy:
  [
     {
        name:'Hair Spa',
        price:250,
        },
        {
           name:'Pedicure',
           price:250,
        },
        {
           name:'Four Hand Hawaiian',
           price:50,
        },
        {
           name:'Manicure',
           price:2500, 
        }
  ],
  Body:
  [
     {
        name:'Body Wrap',
        price:2500,
        },
        {
           name:'Body Scrub',
           price:250,
        },
        
  ],
       image:{
          img1:ac,
          img2:eye,
          img3:green,
          img4:ac,
      }
      },


      {
         id:'25',
         Categorylistmain:'Men',
         locationstreet:"Jp Nagar",
         categorylisttype:'Coming Soon',
         categorylisttypeimg:mcoming,
         name: 'XSALONCE Unisex Salon & Spa',
          type: 'Unisex',
          ischecked:false,
          titlecagory:'Ines Lobo',
       Booking:'Instant Booking',
          clock:'open to 10am',
          catimage:set,
         address:'No 26/1, Thiruverkadu Road, Ayapakkam, Ambattur, Chennai, 600077',
         pincode:'',
     
     
     
         
        timings:[
     
           {
              times:"10:00am"
           },
           {
              times:"11:00am"
           },
           {
              times:"12:00am"
           },
           {
              times:"1:00pm"
           },
           {
              times:"2:00pm"
           },
            {
              times:"3:00pm"
           },
            {
              times:"4:00pm"
           },
            {
              times:"5:00pm"
           },
            {
              times:"6:00pm"
           },
            {
              times:"7:00pm"
           },
            {
              times:"8:00pm"
           },
            {
              times:"9:00pm"
           },
            {
              times:"10:00pm"
           },
            {
              times:"11:00pm"
           },
     
           {
              times:"12:00pm"
           }
     
        ],
         categorytype:'Products',
          service:'155 ',
          googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
          About:'Book your services and avail the Online offers from the outles or get 25% Off on any services by booking online. Xsalonce Unisex Salon & Spa provides you the ultimate beauty experience. Their aim is to make you feel better and look beautiful. They have a team of professionals who are always at your service to provide you guidance and treatments according to your skin requirements. They also use premium products and best equipment to ensure quality service in every sense.',
          weekdays:[
             {
     
                name:'Monday',
                time:'10:00am - 9:00pm'
             },
             {
     
                name:'Tuesday',
                time:'10:00am - 9:00pm'
             },
             {
     
                name:'Wednesday',
                time:'10:00am - 9:00pm'
             },
             {
     
                name:'Thursday',
                time:'10:00am - 9:00pm'
             },
             {
     
                name:'Friday',
                time:'10:00am - 9:00pm'
             },
             {
     
                name:'Saturday',
                time:'10:00am - 9:00pm'
             },
             {
     
                name:'Sunday',
                time:'10:00am - 9:00pm'
             },
             
         ],
         Hairservice:[
             
           {
        
              name:'Eye Brow Threading',
              price:20,
          },
         
           {
              name:'Cheryles Derma shade (50) SPF',
              price:600,
          },
          {
              name:'Cheryls DermaLite',
              price:300,
          },
          {
              name:'Loreal Prodect',
              price:900,
          },
          {
              name:'INOVA POST MASK',
              price:40,
          },
          {
              name:'INOVA POST COLOUR SHAMPOO',
              price:100,
          }
       ],
     
       MassageTherapy:
     [
        {
        name:'5 Min Express Massage therapy',
        price:2500,
        },
        {
           name:'Signature Massage',
           price:250,
        },
        {
           name:'Four Hand Hawaiian',
           price:550,
        },
        {
           name:'Signature Massage',
           price:2500, 
        }
     ],
     HygieneTherapy:
     [
        {
           name:'Hair Spa',
           price:250,
           },
           {
              name:'Pedicure',
              price:250,
           },
           {
              name:'Four Hand Hawaiian',
              price:50,
           },
           {
              name:'Manicure',
              price:2500, 
           }
     ],
     Body:
     [
        {
           name:'Body Wrap',
           price:2500,
           },
           {
              name:'Body Scrub',
              price:250,
           },
           
     ],
          image:{
             img1:light,
             img2:eye,
             img3:green,
             img4:ac,
         }
         },


         
{

   id:'26',
   locationstreet:"Jp Nagar",
   titlecagory:'Ines Lobo',
   ischecked:false,
    name: 'Never End Salon Spa',
    type: 'Unisex',
    Categorylistmain:'Kids',
    categorylisttype:'Coming Soon',
    categorylisttypeimg:kcoming,
    Booking:'Instant Booking',
    clocks:'open to 10am',
   address:'Never End Salon Spa Salon/Studio in Baner, Baner, Pune, Maharashtra, India, Ofc 10,11,12 opp to prime rose mall, Pune',
   pincode:'',
   About:'Never End Salon Spa Salon/Studio in Baner, Baner, Pune, Maharashtra,',
   categorytype:'Beard',
   catimage:heart,
    service:'245',
    timings:[
        {
        times:"10:00am"
     },
     {
        times:"11:00am"
     },
     {
        times:"12:00am"
     },
     
     {
        times:"1:00pm"
     },
     {
        times:"2:00pm"
     },
      {
        times:"3:00pm"
     },
      {
        times:"4:00pm"
     },
      {
        times:"5:00pm"
     },
      {
        times:"6:00pm"
     },
      {
        times:"7:00pm"
     },
      {
        times:"8:00pm"
     },
      {
        times:"9:00pm"
     },
      {
        times:"10:00pm"
     },
      {
        times:"11:00pm"
     },

     {
        times:"12:00pm"
     }

  ],
    googlemap:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66155298887!2d77.35005092310553!3d12.954516271414837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1571217279413!5m2!1sen!2sin',
    image:{
       img1:chair,
       img2:eye,
       img3:green,
       img4:ac,
   },
   weekdays:[
       {

          name:'Monday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Tuesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Wednesday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Thursday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Friday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Saturday',
          time:'10:00am - 9:00pm'
       },
       {

          name:'Sunday',
          time:'10:00am - 9:00pm'
       },
   ],
   Hairservice:[
       
     {
  
        name:'Eye Brow Threading',
        price:20,
    },
   
     {
        name:'Cheryles Derma shade (50) SPF',
        price:600,
    },
    {
        name:'Cheryls DermaLite',
        price:300,
    },
    {
        name:'Loreal Prodect',
        price:900,
    },
    {
        name:'INOVA POST MASK',
        price:40,
    },
    {
        name:'INOVA POST COLOUR SHAMPOO',
        price:100,
    }
 ],

 MassageTherapy:
[
  {
  name:'5 Min Express Massage therapy',
  price:2500,
  },
  {
     name:'Signature Massage',
     price:250,
  },
  {
     name:'Four Hand Hawaiian',
     price:550,
  },
  {
     name:'Signature Massage',
     price:2500, 
  }
],
HygieneTherapy:
[
  {
     name:'Hair Spa',
     price:250,
     },
     {
        name:'Pedicure',
        price:250,
     },
     {
        name:'Four Hand Hawaiian',
        price:50,
     },
     {
        name:'Manicure',
        price:2500, 
     }
],
Body:
[
  {
     name:'Body Wrap',
     price:2500,
     },
     {
        name:'Body Scrub',
        price:250,
     },
     
],
},
         

]




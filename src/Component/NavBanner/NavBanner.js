import React, { Component } from 'react'
import { ProductConsumer } from '../../Context';
import { Link } from 'react-router-dom';
export default class NavBanner extends Component { 
  render () {
    return (
        <ProductConsumer>
            {((value) => {
const {bannerclose,banclose } = value;
return (
 <>
  {
banclose?
<div className="banner-Main">
<div className="banner-style" >
<div className="d-flex">
<p>Get Your Next Haircut For Free</p>
<button className="banner-btn">
<Link to="/login">Join Today</Link></button>
</div>
<i class="fas fa-times n-font-asw" onClick={bannerclose}></i>
</div>
</div>
:null
}
</>
);
})}
  </ProductConsumer>
            )
}
}

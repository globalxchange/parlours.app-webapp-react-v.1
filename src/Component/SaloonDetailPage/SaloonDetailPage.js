import React, { Component } from 'react';
import { ProductConsumer } from '../../Context';
import {Carousel,Modal,Button} from 'react-bootstrap';
import './SaloonDetailPage.css'
import rating from '../../Img/star.png'
import Madolgoogle from '../../Component/Madolgoogle/Madolgoogle'
import Shock from '../../Img/shock.png'
import men from '../../Img/men.png'
import clock from '../../Img/clock.png'
import map from '../../Img/map.png'
import HomeNav from '../HomeNav/Detailnav'
import DatePicker from "../Datapicker/Datapicker";
import {countryCodeApi} from '../CountryCodeApi/CountryCodeApi'
import StickyFooter from 'react-sticky-footer'
export default class SaloonDetailPage extends Component {
  
render() {
 return (
 <ProductConsumer>
                {((value) => {
                   const {SaloonName, service,images,showDate1 , address,handleShow,showResults, locationmap,
                    handleClose,show,Hairservice,check ,showCheckbox,About,Body, MassageTherapy, HygieneTherapy, weekdays, selected,inputValue,inputValue1,type,
                    Booking,clocks,timings,showdatefunc,ser,showDate,showmobilefunc,showhead,showmobilefuncprev,showmobile, showdatefuncprev  } = value;
                      let sum=0;
                    selected.forEach(val  => {
                      sum += val.price
                    })
                    console.log("ajko", MassageTherapy)
 return (
                        <>
                        <div className="stick" >
                        <HomeNav />
                        </div>
                        <div style={{paddingTop:"4rem",
                            overflow: "hidden",
                      }} className="cotainer" >
                            <div className="row ">
                                <div className="col-md-4  detail">
                                <div className="d-t">
                          <h1 className="sh">{SaloonName}</h1>
                    <p className="ad">{ address}  <Madolgoogle/></p>
                 <p className="ad"><span className="sv">{ service}</span>Services available</p>
                          {/* <img className="star"src={rating} alt="rae"/>
                          <p className="ad">no review</p> */}
                        <Button className="mt-3" variant="primary" onClick={handleShow}>
                         BOOK NOW
      </Button>
      </div>
    <Modal show={show} onHide={handleClose}>
                               <Modal.Header closeButton>
                    <Modal.Title>
                      
                      <>{ showDate1  ? <div className="d-flex">
                      <i class="fas fa-chevron-left mnm mr-5 pt-2" onClick={showdatefuncprev}></i> 
                      <h1 className="sh">Select services</h1>
                      </div> :null}
                      <>
                      {ser ? 
                       <h1 className="sh"> Select services</h1>
                       :null}
                       </>
                     <>
                       { showhead ?
                       <div className="d-flex">
                      <i class="fas fa-chevron-left mnm mr-5 pt-2" onClick={showmobilefuncprev}></i> 
                       <h1  className="sh">Review and confirm</h1>
                       </div>: null
                       }
                       </>
                       </>
                      </Modal.Title>
                               </Modal.Header>
                               <Modal.Body>
 <div className="modelsection">
     <div className="row">
       {/* data */}
       <>
<div className="col-md-6">
{ showmobile ?
<div className="moble">
<div>
<h3 className="pb-5">Mobile Number</h3>
  <p className="pb-3">Please provide your mobile number to confirm your appointment booking.</p>
<div className="mt-3  inputBoxStyle">
<select className="country-pro"  name="countryCode"  required>
{
countryCodeApi.map(countryCode=>{
return (
  <option key={countryCode.dial_code+Math.random()} value={countryCode.dial_code}>{countryCode.name+" "+countryCode.dial_code}</option>
)
})
}
</select>
<input type="number" placeholder="Enter Your Phone Number" name="phoneNumber" onChange={inputValue1} autoComplete="off" className="form-control customFormControlSelect" required/>
</div> 
<Button className="mt-5 " variant="primary" >
                        Continue
      </Button> 
</div>
</div>
:null
}


                                        { showDate  ?   
                        <div className="mj">
<div>
  <DatePicker/> 
 <div>
  {
    timings.map(ti=>{
      return(
        <div className="timeing-sec" onClick={showmobilefunc }>
        <p>{ti.times}</p>
        <i class="fas fa-chevron-right"></i>
        </div>
      )
    })
  }
</div> 
</div>
   </div>
 :   ""
 }

                         {showCheckbox ? <>
                                    <div className=" mj">
                                       <h3 className="pb-3">Hair</h3>
                                 {
                                   Hairservice.map(i => {
                                     return (
                                       <>
                                          <label class="custom-checkbox">
                                         <div className="serv">
                                      <input type="checkbox" name={check} onChange={inputValue.bind(this, i)} />
                                         <span></span>
                                           <h5>{i.name}</h5>
                                           <h5>Rs{i.price}</h5>
                                         </div>
                                         </label>
                                       </>
                                     )
                                   })
                                 }

<h3 className="pt-3 pb-3"> MassageTherapy</h3>
{
  MassageTherapy.map(mt => {
                                     return (
                                       <>
                                        <label class="custom-checkbox">
                                         <div className="serv">
                                         <input type="checkbox" name={check} onChange={inputValue.bind(this, mt)} />
                                         <span></span>
                                           <h5>{mt.name}</h5>
                                           <h5>Rs{mt.price}</h5>
                                         </div>
                                         </label>
                                       </>
                                     )
                                   })
                                 }

                                 <h3 className="pt-3 pb-3"> HygieneTherapy</h3>
                                 {
                                     HygieneTherapy.map(hy => {
                                      return (
                                        <>
                                         <label class="custom-checkbox">
                                          <div className="serv">
                                          <input type="checkbox" name={check} onChange={inputValue.bind(this, hy)} />
                                          <span></span>
                                            <h5>{hy.name}</h5>
                                            <h5>Rs{hy.price}</h5>
                                          </div>
                                          </label>
                                        </>
                                      )
                                    })
                                 }
                                 <h3>Body & Spa</h3>
                                 {
                                   Body.map(hy => {
                                    return (
                                      <>
                                       <label class="custom-checkbox">
                                        <div className="serv">
                                        <input type="checkbox" name={check} onChange={inputValue.bind(this, hy)} />
                                        <span></span>
                                          <h5>{hy.name}</h5>
                                          <h5>Rs{hy.price}</h5>
                                        </div>
                                        </label>
                                      </>
                                    )
                                  })
                                 
                                }
                                 </div>
                              
                                 <div style={{margin:"auto"}}>
                                 { showResults ?  
                                
                                 <Button  style={{width: "80%"}}variant="primary  yt" onClick={showdatefunc}>
                                  BOOK NOW 
          </Button>
       
          : null }
          </div>
          </> : ""}
                        
                          
          {/* Date section */}
   
        {/*mobile  */}
      
</div>

</>
<div className="col-md-6 ">
  <div className="mj">
 <div className="">
<img className="io" src={images.img1} alt=""/>
<div className="sel">
  <h2 className="pb-3">{SaloonName}</h2>
{
                                   selected.map(e => {
                                     return (
                                       <>
                                       <div className="serv">
                                         <h5>{e.name}</h5>
                                         <h5>{e.price}</h5>
                                         </div>

                                       </>
                                     )
                                   })
                                 }

                                  </div>
                                  

</div>
</div>
{ showResults ?  
<h1 className="pt-3 pb-3">
 Total: {sum}
 </h1>: null}

</div>

                               
                                 <div>
 </div>
                                 </div>
                                 </div>

                               </Modal.Body>
                      
                             </Modal>
                                </div>
                                <div className="col-md-6 p-0" >
                                <Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={images.img1}
      alt="First slide"
    />
 </Carousel.Item>
  <Carousel.Item>
  <img
      className="d-block w-100"
      src={images.img2}
      alt="First slide"
    />
</Carousel.Item>
  <Carousel.Item>
  <img
      className="d-block w-100"
      src={images.img3}
      alt="First slide"
    />

  </Carousel.Item>
  <Carousel.Item>
  <img
      className="d-block w-100"
      src={images.img4}
      alt="First slide"
    />

  </Carousel.Item>
</Carousel>

                                </div>
                            </div>
                            <div className="container">
                            <div className="row kl">
                            <div className="col-md-8">
                              <div className="mainAboutSection">
<h1 >About us</h1>
<p className="ad">{About}</p>
<div className="tr">
  <div className="pg">

    <img src={Shock} alt="logo"/>
    <p>
{Booking}
    </p>
    </div>
    <div className="pg">

    <img src={men} alt="logo"/>
    <p>
{type}
    </p>
    </div>
    <div className="pg">

    <img src={clock} alt="logo"/>
    <p>
{clocks}
    </p>
    </div>
    </div>
</div>
                            </div>
                            <div className="col-md-4">

<h1>Location</h1>
<div className="d-flex">
<p className="ad">{ address}  <Madolgoogle/></p>
  <img className="ht" src={map} alt=""/>
</div>

<div className="mt-3">
<h1>Opening Hours</h1>
  {
    weekdays.map(w=>{
      return(
        <>
        <div>
        
       
        <div className="weekdays">

          <p className="ps1">{w.name}:</p>
          <p className="ps">{w.time}</p>
        </div>
        </div>
        </>
      )
    })
  }
</div>
                            </div>
                        </div>
                        </div>
                        </div>

                        <StickyFooter
    bottomThreshold={0}
    normalStyles={{
      width:"100%",
      paddingTop: "9px",
      paddingBottom:"9px",
      paddingLeft: "15px",
      paddingRight: "15px",
      borderTop: "1px solid #0000003b",
      backgroundColor: "white",
    }}
    stickyStyles={{
      width:"100%",
      paddingTop: "9px",
      paddingBottom:"9px",
      paddingRight: "15px",
      paddingLeft: "15px",
      borderTop: "1px solid #0000003b",
    backgroundColor: "white",
    bottom: "3px !important",
  
    }}
>
   <div className="footer-section">

     <div >
     <h1 style={{fontSize:"21px"}}className=" mb-0">{SaloonName}</h1>
                 <p className="ad mb-0"><span className="sv">{ service}</span>Services available</p>
                 <div className="footer-section1">
                          <img className="star"src={rating} alt="rae"/>
                          <p className="ad mb-0">no review</p>
                          </div>
         
     </div>
    <div>
    <Button className="mt-3 " variant="primary" onClick={handleShow}>
    BOOK NOW
    </Button>

      </div>
   </div>
</StickyFooter>
                        </>
                    )
                })}
            </ProductConsumer>
        )
    }
}

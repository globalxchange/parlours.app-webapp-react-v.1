import React, { Component } from 'react'
import {Saloon} from '../SaloonApi/SaloonAPi'
import { ProductConsumer } from '../../Context'
import {Modal,Tab,Row,Col,Nav} from 'react-bootstrap'
import './Category.css'
import {Link} from 'react-router-dom'


export default class Category extends Component {
    render() {
      return (
        <ProductConsumer>
                       {((value) => {
                          const { SaloonHairData,  AllCategoryFuc,category_id,   NamehandleClick,NamehandleClick7,NamehandleClick6,NamehandleClick5,locationmap,googlemapshow,show1,cid,category,handleC,inputmapclick,handleS,show} = value;
                          let pp = Saloon.filter( (ele, ind) => ind === Saloon.findIndex( elem => elem.categorytype === ele.categorytype))
                          console.log(pp)
        return (
          <>
  <div className="category-Main_section">

          <div className="container">
                <div className="category yre">
                                {
                                   SaloonHairData.map(c=>{
                                     let SaloonCatoryImage=`https://rendezapp.herokuapp.com/barbers/v1/category/icon/${c.category_uuid}/${c.category_icon_id}`
                                        return(
  <>
                                            <div className="c-list"  onClick={()=>AllCategoryFuc(c)}>
                                     {/* <Link to="/Fliter"  onClick={()=>NamehandleClick(c)} >   */}
                                     {/* <SVG src={c.catimage}
                                      preProcessor={code => code.replace(/fill=".*?"/g, 'fill="currentColor"')}
                                      /> */}
<img className="i-p-o"src={SaloonCatoryImage} alt="logo"/>
                                            <h5 className="mnm" >{c.category_name} </h5>
                                         {/* </Link> */}
                                            </div>
                                            </>
                                        )
                                    })
                                }
                            </div>
</div>
</div>
 {/* see swtich flodder */}
           </>

        )

                       })}
                         </ProductConsumer>  
                       
                       )
    }
}

import React,{useState} from 'react'
import sidebanner from '../Logo/sidebanner.png'
import play from '../Logo/play.png'
import logo from '../Logo/logo.png'
import burger from '../Logo/burger.png'
import s1 from '../Logo/s1.png'
import s2 from '../Logo/s2.png'
import s3 from '../Logo/s3.png'
import down from '../Logo/down.png'
import './HairDashboard.scss'
export default function HairDashboard() {
  const [show,setshow]=useState("")
  const [datainfo,setdatainfo]=useState("")
  const cards=[
    {
      img:s1,
      name:"Invest In The Ecosystem",
      mobile:"Tokenize You’re Hair Style And Get Paid Each Time Someone Mirrors It."
    },
    {
      img:s2,
      name:"Join As A Stylist",
      mobile:"Tokenize You’re Hair Style And Get Paid Each Time Someone Mirrors It."
    },
    {
      img:s3,
      name:"Add You’re Barbershop Or Salon",
      mobile:"Tokenize You’re Hair Style And Get Paid Each Time Someone Mirrors It."
    },
  ]

  const datafuntion=(e)=>{
    setshow(e)
    setdatainfo(e)
  }
  return (
    <div className="dahboard-hair">

      <div className='h-navbar'>
<div className="first">
<a id="hamburger-icon" href="#" title="Menu">
      <span class="line line-1"></span>
      <span class="line line-2"></span>
      <span class="line line-3"></span>
    </a>

   
    <img className='logo' src={logo} alt="" />
</div>
<div className="last">
<label className='f-label'>Get Started</label>
<label className='l-label'>Apps</label>
</div>
      </div>

<div className="dash-section" onMouseOver={()=>datafuntion("")}>
<div className='section-one'>
<h1>Bringing <span> NFT’s </span>To The</h1>
<h2>Beauty Industry</h2>
<h5>Beauty </h5>
<h4>Industry</h4>
<p>Tokenize Your Hair Style And Get Paid Each Time </p>
<p>Someone Mirrors It.</p>

<h3>Tokenize You’re Hair Style And Get Paid Each Time. Someone Mirrors It.</h3>
<label htmlFor="">
<img src={play} alt="" />
How Will It Work
</label>
</div>

<div className='section-two'>
<img src={sidebanner} alt="" />
</div>

<div className='downarrow'>
  <a href="#section1">Learn More</a>

  <img src={down} alt="" />
</div>
</div>




<div className='cards' id="section1">
{
  cards.map(item=>{
    return(
      <div className='cards-tite ' style={datainfo===item.name ||datainfo==="" ?{opacity:"1"}:{opacity:"0.4"}} onMouseOver={()=>datafuntion(item.name)} >
        <img src={item.img} alt="" />
   <h5>{item.mobile}</h5>
          <p>{item.name ===show?show:"" }</p>
       
       
      </div>
    )
  })
}
</div>
    </div>
  )
}

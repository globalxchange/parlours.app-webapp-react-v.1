import React, { Component } from 'react'
import './SaloonPage.css'
import { Saloon } from '../SaloonApi/SaloonAPi'
import { Map } from '../Map/Map'
import { ProductConsumer } from '../../Context';
import { Link } from 'react-router-dom';
import { ButtonToolbar, Button, OverlayTrigger, Popover, Form, Modal } from 'react-bootstrap';
import HomeNav from '../HomeNav/HomeNav'
import Footer from '../Footer/Footer'
import Category from '../Category/Category'
import Buuble from '../Buuble/Buuble'
import Ren from '../../Img/ren.png'
import Promo from '../../Img/hp.mp4'
import NavBanner from '../NavBanner/NavBanner'
import Tradinglocation from '../SaloonCategorySection/Tradingloction'
import Trims from '../SaloonCategorySection/Trims'
import Cuts from '../SaloonCategorySection/Cuts'
// import Facials from '../SaloonCategorySection/Facials'
import Massage from '../SaloonCategorySection/Massage'
import Mobilepage from '../MobilePage/MobilePage'
import Typist from 'react-typist'



//  import TypistLoop from 'react-typist-loop'
import { SectionsContainer, Section } from 'react-fullpage';
import { withTheme } from '@material-ui/core';

export default class SaloonPage extends Component {
  render() {
    let options = {
      activeClass: 'active', // the class that is appended to the sections links
      anchors: ['', '', '', '', '', ''], // the anchors for each sections
      arrowNavigation: true, // use arrow keys
      className: 'SectionContainer', // the class name for the section container
      delay: 1700, // the scroll animation speed
      navigation: false, // use dots navigatio
      scrollBar: false, // use the browser default scrollbar
      sectionClassName: 'Section', // the section class name
      sectionPaddingTop: '0', // the section top padding
      sectionPaddingBottom: '0', // the section bottom padding
      verticalAlign: false // align the content of each section vertical
    }
    return (
      <ProductConsumer>
        {((value) => {
          const { Mimages, serviceclassName, className, navbarclass, showwindow, handleCwindow, googlemapshow, locationmap, HanderClick, inputmapclick, NamehandleClick, handleShow, handleClose, show, NamehandleClick1, NamehandleClick2, result1 } = value;
          let pp = Saloon.filter((ele, ind) => ind === Saloon.findIndex(elem => elem.categorytype === ele.categorytype))
          console.log(pp)

          return (
            <>
              <NavBanner />
              <div className={navbarclass}  >
                <HomeNav />
                <div className={className}>
                  <Buuble />
                </div>
              </div>


              <SectionsContainer className="" {...options}>
                <Section className="custom-section">
                  <div class="container-pp">
                    <section class="area">
                      <div className="Barber-main-page">
                        <div className="Barber-main-page-sub" >
                          <div className={serviceclassName}>
                            <div class="background-wrap">
                              <video id="video-bg-elem" preload="auto" autoplay="true" muted loop>
                                <source src={Promo} type="video/mp4" />Video not supported</video> 
                            </div>
                            <div className="Se-bck">
                              <div className="container containers-p">

                                {/*CountrylocationSection */}
                                <div className="col-md-7">
                                   {/* <TypistLoop interval={3000}>
                                    {[
                                      'Meet Your Best Self',
                                      'Express Yourself',
                                      'Look Your Best',
                                    ].map(text => <Typist key={text} startDelay={1000}>{text}</Typist>)}
                                  </TypistLoop>  */}
                                  {/* <h1 className="Expert-Service">Find The Best Barbers In {locationmap}</h1> */}
                                  <p className="Reliable">All The World's Stylists At You Fingertip</p>
                                  <div className="search-section">
                                    <div className="qw m-po-e ">
                                      <ButtonToolbar>
                                        {['bottom'].map(placement => (
                                          <OverlayTrigger
                                            trigger="click"
                                            rootClose={true}
                                            key={placement}
                                            rootCloseEvent='click'
                                            placement={placement}
                                            overlay={
                                              <Popover className="popsearch-p" id={`popover-positioned-${placement}`}>
                                                {/* <Popover.Title as="h3">{`Popover ${placement}`}</Popover.Title> */}
                                                <Popover.Content>
                                                  <div className="category1">
                                                    <div className="search-field-f ">
                                                      {
                                                        result1.map(r => {
                                                          return (
                                                            <div className="d-flex selection-section">
                                                              <Link to="/Fliter" onClick={() => NamehandleClick2(r)} >
                                                                <div className="d-flex mb-3 alignItem" >
                                                                  <img className="res-img" src={r.image.img1} alt="" />
                                                                  <h5 className="iot">{r.name}</h5>
                                                                </div>
                                                              </Link>
                                                            </div>
                                                          )
                                                        })
                                                      }
                                                    </div>
                                                  </div>

                                                </Popover.Content>
                                              </Popover>
                                            }
                                          >
                                            <div>
                                              <Form.Control size="lg" type="text" className="search-input-i" onChange={NamehandleClick1} placeholder="Search For a Service" />
                                              <p class="eg">Example: Barbershops In New York, Salons In Moscow</p>
                                            </div>
                                          </OverlayTrigger>
                                        ))}
                                      </ButtonToolbar>
                                    </div>
                                    <div className="qw ">
                                      <i class="m-r-a  uy search-location-bar fas fa-search"></i>
                                      <Button className="btn-search-i" onClick={handleShow}>
                                        <img className="img-location " src={Mimages} alt="lofo" />
                                        {locationmap}
                                      </Button>
                                      {googlemapshow ?
                                        <Modal className="map-p" show={show} onHide={handleClose} animation={false}>
                                          <Modal.Body>
                                            <div className="container p-pl" >
                                              <h1 className="country-h1">Select your city</h1>
                                              <p className="ind">INDIA</p>
                                              <div className="map-j">
                                                {
                                                  Map.map(m => {
                                                    return (
                                                      <div className="sub-map-j" onClick={() => inputmapclick(m)}>
                                                        <img src={m.Mimages} alt="country" />
                                                        <p >{m.Mname}</p>
                                                      </div>
                                                    )
                                                  })
                                                }
                                              </div>
                                            </div>
                                          </Modal.Body>
                                        </Modal>
                                        : null}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          < Category />
                        </div>
                        <div>

                        </div>
                      </div>

                    </section>
                  </div>

                </Section>

                <Section >

                  <div className="main-d-section">
                    <Tradinglocation />
                  </div>

                </Section>
                <Section >
                  <div className="main-d-section">

                    <Cuts />
                  </div>
                </Section>
                <Section >
                  <div className="main-d-section">
                    <Trims />
                  </div>
                </Section>
                <Section >
                  <div className="main-d-section">
                    <Massage />

                  </div>
                </Section>
                <Section >
                  <div className="main-d-section1">
                    <div className="footersection-sub">

                      <Mobilepage />
                      <Footer />
                    </div>

                  </div>
                </Section>
                {/* <div className="s-m-b">
                <div className="container">
                  <div className="row">
                    <div className="col-md-6">
                      <img className="mobile" src={Mobile} alt="lo" />
                    </div>
                    <div className="col-md-6 klo">
                      <div>
                        <h3 className="download">Download the App</h3>
                        <p className="mlj">Choose and book from 100+ services and track them on the go with the Haircuts.com App</p>

                        <p className="send">Send a link via SMS to install the app</p>
                        <div className="mobile-fle">

                          <p className="mb-num">+91</p>
                          <Form.Control size="lg" type="text" className="impu-moble" onChange={NamehandleClick1} placeholder="" />
                          <button className="bt-mob">Text App Link</button>
                        </div>
                        <img className=" ios" src={ios} alt="lo" />
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}




                {/*  */}
                {/* <Button className="d" onClick={handlewindow}>
       hahahah
      </Button> */}

                <Modal className="popupsignup" show={showwindow} onHide={handleCwindow} animation={false}>
                  <Modal.Body>
                    <div className="pop-sign-main">
                      <img className="renimg" src={Ren} alt="" />
                      <h1>Looking Good Requires</h1>
                      <h1>A Free Account </h1>
                      <button className="btn-sign-up-pa1" onClick={handleCwindow}>Learn Why</button>
                    </div>
                    <div className="sing-min-page-text">
                      <p className="enjoy">Create A Free Account Today To Enjoy:</p>
                      <div className="sing-cotent">
                        <i class="fas fa-check-circle io-svg"></i>
                        <p>Two Complimentary Haircuts At Your Favorite Barber</p>
                      </div >
                      <div className="sing-cotent">
                        <i class="fas fa-check-circle io-svg"></i>
                        <p>Monthly Serving Of Your Favorite Product</p>
                      </div>
                      <div className="sing-cotent">
                        <i class="fas fa-check-circle io-svg"></i>
                        <p>Free Home Visits </p>
                      </div>
                      <button className="btn-sign-up-pa" onClick={handleCwindow} style={{color:'white'}}>
                      <Link to="/login">Sign Up</Link></button>
                    </div>

                    <div>

                      <p className="alred-text">Already have a account ? <span className="login-text"><Link to="/login">Log In</Link></span></p>
                    </div>
                  </Modal.Body>
                </Modal>
              </SectionsContainer>
            </>
          )

        })}
      </ProductConsumer>

    )
  }
}


// const MainPage=()=>{
// return
// (
//     <>
// <div>
//                {
// SaloonAi.map(items=>{
//     return(
//         <h1>{items.name}</h1>
//     )
// })
//                 }

// </div>
// </>
// )
// }
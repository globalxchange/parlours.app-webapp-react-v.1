import React, { Component } from 'react';
import './Lagin.css'
//import imgs from ''
export default class Lagin extends Component {
    render() {
        return (
            <div>
                <div className="login-full">
                    <div className="login-in-box">
                        <div className="login-left-panel">
                            <div className="login-health-hp">
                                <h2 className="sam-title-font mb-3" >Hey Good Looking</h2>
                                <p className="sam-subtext-font mb-4">Login To Haircuts.com</p>
                            </div>
                            <form className="form-full-ep">
                                <div className="form-group">
                                    <input type="email" name="logemail" className="form-control" aria-describedby="emailHelp" placeholder="Enter Email" required="" />

                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control" placeholder="Password" name="logpassword" required="" />
                                </div>
                                <div className="text-center pt-3 btn-link-log login-btn sam-title-font "><a  href="https://haircuts.com"><button disabled="" type="submit" className="btn login-btn sam-title-font">LOG IN</button></a>
                                </div>
                            </form>
                            <div className="mt-3">
                                <a className="sam-font" href="/resetpassword">Forgot Password?</a>
                            </div><div className="get-start-acc">
                                <p className="sam-font mb-0">If You Don't Have An Account, Click Here<a href="/pricing">  <i className="fas fa-arrow-right fa-arr-rt"></i></a>

                                </p>

                            </div>
                        </div>
                        <div className="login-right-panel">
                            
                        </div>
                    </div>
                </div>
            </div>
           

        )
    }
}
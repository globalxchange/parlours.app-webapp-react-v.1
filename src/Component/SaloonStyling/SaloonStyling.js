import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import { ProductConsumer } from '../../Context'
import './SaloonStyling.css'
import {ListGroup} from 'react-bootstrap'
import HomeNav from '../HomeNav/HomeNav'
import Buuble from '../Buuble/Buuble'
import NavBanner from '../NavBanner/NavBanner'
export default class SaloonStyling extends Component {
 render () {
 return (
        <ProductConsumer>
            {((value) => {
                const {SaloonStyleObj,navbarclass,className,} = value;
    return (
     <>
     <NavBanner/>
     <div   className={navbarclass}  >
     <HomeNav />
     <div className={className}>
     
       <Buuble />
     
       </div>
     </div>

<div className="stylingcss">
     <div className="container">
         <h1>Hey Ronaldo, what type of hair</h1>
         <h1>color do you currently have </h1>
    <ListGroup className="list-g">
       {
           SaloonStyleObj.map(items=>
            {
                return(
                    <ListGroup.Item action  className="list-styling-sec " >{items.list}</ListGroup.Item>
                  
                )
            })
       }
  </ListGroup>
  <button className="but-nxt">
      Next
  </button>
  </div> 
  </div>
</>
);
})}
  </ProductConsumer>
            )
}
}
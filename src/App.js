import React,{useState, Component } from 'react';
import { Switch, Route ,BrowserRouter} from "react-router-dom";
import Alerts from './Component/HairDashboard/HairDashboard'
import Indexpage from './Component/SaloonPage/SaloonPage'
  import './App.css'
  import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

return(
  <React.Fragment>
 <div className="App-contianer">
 

 

    <Switch>
    <Route
    exact
        path="/"
        render={() => (
          <Alerts/>
         
        )}
      /> 
    <Route
    exact
        path="/legacy"
        render={() => (
          <Indexpage/>
         
        )}
      /> 
     
    
    </Switch>
  </div>
</React.Fragment>
  
 
   
  );
}

export default App;
